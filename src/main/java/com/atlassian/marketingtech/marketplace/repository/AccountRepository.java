package com.atlassian.marketingtech.marketplace.repository;

import org.springframework.data.repository.CrudRepository;

import com.atlassian.marketingtech.marketplace.domain.Account;

/**
 * 
 * @author selumalai JPA repository class to access Account Class in Derby DB
 *
 */

public interface AccountRepository extends CrudRepository<Account, String> {
	public Account findByCompanyName(String companyName);

}
