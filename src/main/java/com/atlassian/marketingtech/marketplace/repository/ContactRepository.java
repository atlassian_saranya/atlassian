package com.atlassian.marketingtech.marketplace.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.atlassian.marketingtech.marketplace.domain.Contact;

/**
 * 
 * @author selumalai
 * JPA repository class to access Contact Class in Derby DB
 *
 */

public interface ContactRepository extends CrudRepository<Contact, String> {
	public List<Contact> findAllByAccountCompanyName(String accountName);
	
	public Contact findByContactName(String contactName);
	
	

}
