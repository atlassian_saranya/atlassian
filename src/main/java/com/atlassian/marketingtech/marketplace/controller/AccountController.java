package com.atlassian.marketingtech.marketplace.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.atlassian.marketingtech.marketplace.constant.AtlaasianContant;
import com.atlassian.marketingtech.marketplace.domain.Account;
import com.atlassian.marketingtech.marketplace.service.AccountService;

/**
 * 
 * @author selumalai Controller Class which contains API end points to access
 *         Account JPA resource.
 *
 */
@Controller
public class AccountController {
	@Autowired
	private AccountService accountService;

	private static Logger log = Logger.getLogger(AccountController.class);

	@RequestMapping(value = "/account", method = RequestMethod.GET, produces = { "application/json" })
	private @ResponseBody List<Account> getAllAccountObjects() throws Exception {
		log.info("account get call");
		return accountService.retrieveAllAccounts();
	}

	@RequestMapping(value = "/account/{accountName}", method = RequestMethod.GET, produces = { "application/json" })
	private @ResponseBody Account getAccountObject(@PathVariable("accountName") String accountName) throws Exception {
		return accountService.retrieveAccount(accountName);
	}

	@RequestMapping(value = "/account", method = RequestMethod.POST, consumes = "application/json")
	private @ResponseBody void persistAccountObject(@RequestBody String payload) {
		try {
			JSONObject accountJSONObject = new JSONObject(payload);
			Account account = new Account();
			account.setAddressLine1(accountJSONObject.getString(AtlaasianContant.ADDRESS1));
			account.setAddressLine2(accountJSONObject.getString(AtlaasianContant.ADDRESS2));
			account.setCity(accountJSONObject.getString(AtlaasianContant.CITY));
			account.setState(accountJSONObject.getString(AtlaasianContant.STATE));
			account.setPostalCode(accountJSONObject.getString(AtlaasianContant.POSTAL_CODE));
			account.setCountry(accountJSONObject.getString(AtlaasianContant.COUNTRY));
			account.setCompanyName(accountJSONObject.getString(AtlaasianContant.COMPANY_NAME));
			accountService.saveAccount(account);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

	@RequestMapping(value = "/account/{accountName}", method = RequestMethod.PUT, consumes = "application/json")
	private @ResponseBody void updateAccountObject(@RequestBody String payload,
			@PathVariable("accountName") String accountName) {
		try {
			JSONObject accountJSONObject = new JSONObject(payload);
			Account account = new Account();
			account.setAddressLine1(accountJSONObject.getString(AtlaasianContant.ADDRESS1));
			account.setAddressLine2(accountJSONObject.getString(AtlaasianContant.ADDRESS2));
			account.setCity(accountJSONObject.getString(AtlaasianContant.CITY));
			account.setState(accountJSONObject.getString(AtlaasianContant.STATE));
			account.setPostalCode(accountJSONObject.getString(AtlaasianContant.POSTAL_CODE));
			account.setCountry(accountJSONObject.getString(AtlaasianContant.COUNTRY));
			account.setCompanyName(accountName);
			accountService.saveAccount(account);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

}
