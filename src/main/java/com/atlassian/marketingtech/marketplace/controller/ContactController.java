package com.atlassian.marketingtech.marketplace.controller;

import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.atlassian.marketingtech.marketplace.constant.AtlaasianContant;
import com.atlassian.marketingtech.marketplace.domain.Account;
import com.atlassian.marketingtech.marketplace.domain.Contact;
import com.atlassian.marketingtech.marketplace.service.AccountService;
import com.atlassian.marketingtech.marketplace.service.ContactService;

/**
 * 
 * @author selumalai Controller Class which contains API end points to access
 *         Contact JPA resource.
 *
 */
@Controller
public class ContactController {
	@Autowired
	private ContactService contactService;
	@Autowired
	private AccountService accountService;

	private static Logger log = Logger.getLogger(ContactController.class);

	@RequestMapping(value = "/contact", method = RequestMethod.GET)
	private @ResponseBody List<Contact> getAllContactObjects() throws Exception {
		return contactService.retrieveAllContacts();
	}

	@RequestMapping(value = "/contact/{contactName}", method = RequestMethod.GET)
	private @ResponseBody Optional<Contact> getContactObject(@PathVariable("contactName") String contactName)
			throws Exception {
		return contactService.retrieveContact(contactName);
	}

	@RequestMapping(value = "/account/{accountName}/contact/{contactName}", method = RequestMethod.GET)
	private @ResponseBody Optional<Contact> getContactObjectForAccount(@PathVariable("accountName") String accountName,
			@PathVariable("contactName") String contactName) throws Exception {
		return contactService.retrieveContact(contactName);
	}

	@RequestMapping(value = "/account/{accountName}/contact", method = RequestMethod.GET)
	private @ResponseBody List<Contact> getAllContactObjectsForAccount(@PathVariable("accountName") String accountName)
			throws Exception {
		return contactService.retrieveAllContactsForAccount(accountName);
	}

	@RequestMapping(value = "/account/{accountName}/contact", method = RequestMethod.POST, consumes = "application/json")
	private @ResponseBody void persistContactObjectForAccount(@RequestBody String payload,
			@PathVariable("accountName") String accountName) {
		try {
			JSONObject contactJSONObject = new JSONObject(payload);
			Contact contact = new Contact();
			Account account = accountService.retrieveAccount(accountName);
			contact.setAddressLine1(contactJSONObject.getString(AtlaasianContant.ADDRESS1));
			contact.setAddressLine2(contactJSONObject.getString(AtlaasianContant.ADDRESS2));
			contact.setCity(contactJSONObject.getString(AtlaasianContant.CITY));
			contact.setState(contactJSONObject.getString(AtlaasianContant.STATE));
			contact.setPostalCode(contactJSONObject.getString(AtlaasianContant.POSTAL_CODE));
			contact.setCountry(contactJSONObject.getString(AtlaasianContant.COUNTRY));
			contact.setContactName(contactJSONObject.getString(AtlaasianContant.CONTACT_NAME));
			contact.setEmail(contactJSONObject.getString(AtlaasianContant.EMAIL));
			contact.setAccount(account);
			contactService.saveContact(contact);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

	@RequestMapping(value = "/contact", method = RequestMethod.POST, consumes = "application/json")
	private @ResponseBody void persistContactObject(@RequestBody String payload) {
		try {
			JSONObject contactJSONObject = new JSONObject(payload);
			Contact contact = new Contact();
			contact.setAddressLine1(contactJSONObject.getString(AtlaasianContant.ADDRESS1));
			contact.setAddressLine2(contactJSONObject.getString(AtlaasianContant.ADDRESS2));
			contact.setCity(contactJSONObject.getString(AtlaasianContant.CITY));
			contact.setState(contactJSONObject.getString(AtlaasianContant.STATE));
			contact.setPostalCode(contactJSONObject.getString(AtlaasianContant.POSTAL_CODE));
			contact.setCountry(contactJSONObject.getString(AtlaasianContant.COUNTRY));
			contact.setContactName(contactJSONObject.getString(AtlaasianContant.CONTACT_NAME));
			contact.setEmail(contactJSONObject.getString(AtlaasianContant.EMAIL));
			contact.setAccount(null);
			contactService.saveContact(contact);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

	@RequestMapping(value = "/account/{accountName}/contact/{contactName}", method = RequestMethod.PUT, consumes = "application/json")
	private @ResponseBody void updateContactObjectForAccount(@RequestBody String payload,
			@PathVariable("accountName") String accountName, @PathVariable("contactName") String contactName) {
		try {
			JSONObject contactJSONObject = new JSONObject(payload);
			Contact contact = new Contact();
			Account account = accountService.retrieveAccount(accountName);
			contact.setAddressLine1(contactJSONObject.getString(AtlaasianContant.ADDRESS1));
			contact.setAddressLine2(contactJSONObject.getString(AtlaasianContant.ADDRESS2));
			contact.setCity(contactJSONObject.getString(AtlaasianContant.CITY));
			contact.setState(contactJSONObject.getString(AtlaasianContant.STATE));
			contact.setPostalCode(contactJSONObject.getString(AtlaasianContant.POSTAL_CODE));
			contact.setCountry(contactJSONObject.getString(AtlaasianContant.COUNTRY));
			contact.setEmail(contactJSONObject.getString(AtlaasianContant.EMAIL));
			contact.setContactName(contactName);
			contact.setAccount(account);
			contactService.saveContact(contact);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

	@RequestMapping(value = "/contact/{contactName}", method = RequestMethod.PUT, consumes = "application/json")
	private @ResponseBody void updateContactObject(@RequestBody String payload,
			@PathVariable("contactName") String contactName) {
		try {
			JSONObject contactJSONObject = new JSONObject(payload);
			Contact contact = new Contact();
			contact.setAddressLine1(contactJSONObject.getString(AtlaasianContant.ADDRESS1));
			contact.setAddressLine2(contactJSONObject.getString(AtlaasianContant.ADDRESS2));
			contact.setCity(contactJSONObject.getString(AtlaasianContant.CITY));
			contact.setState(contactJSONObject.getString(AtlaasianContant.STATE));
			contact.setPostalCode(contactJSONObject.getString(AtlaasianContant.POSTAL_CODE));
			contact.setCountry(contactJSONObject.getString(AtlaasianContant.COUNTRY));
			contact.setEmail(contactJSONObject.getString(AtlaasianContant.EMAIL));
			contact.setContactName(contactName);
			contactService.saveContact(contact);
		} catch (Exception e) {
			log.error(e.getMessage());
		}
	}

}
