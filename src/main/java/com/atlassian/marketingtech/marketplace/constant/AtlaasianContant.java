package com.atlassian.marketingtech.marketplace.constant;

/*
 * @author selumalai 
 * Constants class
 *     
 */

public class AtlaasianContant {
	public static final String ADDRESS1 = "addressLine1";
	public static final String ADDRESS2 = "addressLine2";
	public static final String CITY = "city";
	public static final String STATE = "state";
	public static final String POSTAL_CODE = "postalCode";
	public static final String COUNTRY = "country";
	public static final String EMAIL = "email";
	public static final String ACCOUNT_NAME = "accountName";
	public static final String CONTACT_NAME = "contactName";
	public static final String COMPANY_NAME = "companyName";

}
