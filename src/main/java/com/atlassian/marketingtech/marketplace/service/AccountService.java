package com.atlassian.marketingtech.marketplace.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.atlassian.marketingtech.marketplace.domain.Account;
import com.atlassian.marketingtech.marketplace.repository.AccountRepository;

@Service
public class AccountService {
	@Autowired
	private AccountRepository repoAccount;

	public List<Account> retrieveAllAccounts() {
		List<Account> accountList = new ArrayList<Account>();
		for (Account account : repoAccount.findAll())
			accountList.add(account);
		return accountList;
	}

	public Account retrieveAccount(String accountName) {
		return repoAccount.findByCompanyName(accountName);
	}

	public void saveAccount(Account account) {
		repoAccount.save(account);
	}

}
