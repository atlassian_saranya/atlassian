package com.atlassian.marketingtech.marketplace.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.atlassian.marketingtech.marketplace.domain.Contact;
import com.atlassian.marketingtech.marketplace.repository.ContactRepository;

@Service
public class ContactService {
	@Autowired
	private ContactRepository repoContact;
	
	public List<Contact> retrieveAllContacts(){
		List<Contact> ContactList = new ArrayList<Contact>();
		for(Contact Contact :repoContact.findAll())
			ContactList.add(Contact);
		return ContactList;
	}
	
	public List<Contact> retrieveAllContactsForAccount(String accountName){
		List<Contact> ContactList = new ArrayList<Contact>();
		for(Contact Contact :repoContact.findAllByAccountCompanyName(accountName))
			ContactList.add(Contact);
		return ContactList;
	}
	
	public Optional<Contact> retrieveContact(String ContactName){
		return repoContact.findById(ContactName);
	}
	
	public void saveContact(Contact Contact){
		repoContact.save(Contact);
	}

}
