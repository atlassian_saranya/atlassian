package com.atlaasian.marketingtech.marketplace;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.atlaasian.marketingtech.marketplace.controllertest.AccountControllerServiceUnitTest;
import com.atlaasian.marketingtech.marketplace.controllertest.ContactControllerServiceUnitTest;
import com.atlaasian.marketingtech.marketplace.integrationtest.AccountIntegrationTest;
import com.atlaasian.marketingtech.marketplace.integrationtest.ContactIntegrationTest;
import com.atlaasian.marketingtech.marketplace.repositorytest.AccountRepositoryUnitTest;
import com.atlaasian.marketingtech.marketplace.repositorytest.ContactRepositoryUnitTest;
/**
 * 
 * @author selumalai
 * Test Suite class to run all testcases
 *
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
    AccountRepositoryUnitTest.class, 
    ContactRepositoryUnitTest.class,
    AccountControllerServiceUnitTest.class,
    ContactControllerServiceUnitTest.class,
    AccountIntegrationTest.class,
    ContactIntegrationTest.class
})
public class AtlassianApplicationTests {
	
}
