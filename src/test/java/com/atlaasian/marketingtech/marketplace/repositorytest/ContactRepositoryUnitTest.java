package com.atlaasian.marketingtech.marketplace.repositorytest;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.atlassian.marketingtech.marketplace.AtlassianApplication;
import com.atlassian.marketingtech.marketplace.domain.Account;
import com.atlassian.marketingtech.marketplace.domain.Contact;
import com.atlassian.marketingtech.marketplace.repository.AccountRepository;
import com.atlassian.marketingtech.marketplace.repository.ContactRepository;
/**
 * 
 * @author selumalai
 * Class to test contact repository
 *
 */
@ContextConfiguration(classes = {AtlassianApplication.class})
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class ContactRepositoryUnitTest {

	@Autowired
	private ContactRepository repoContact;
	
	@Autowired
	private AccountRepository repoAccount;
	
	@Before
	public void setUp(){
		
	}

	@Test
	public void testContactPersist() throws Exception {
		Contact contact = new Contact();
		contact.setContactName("Test");
		Contact retrievedContact = repoContact.save(contact);
		assertThat(retrievedContact).hasFieldOrPropertyWithValue("contactName","Test");
	}
	
	@Test
	public void testContactRetrieve() throws Exception {
		Contact contact = new Contact();
		contact.setContactName("Test1");
		contact.setCity("Austin");
		repoContact.save(contact);
		Contact retrievedContact = repoContact.findByContactName("Test1");
		assertThat(retrievedContact).hasFieldOrPropertyWithValue("city","Austin");


	}
	
	@Test
	public void testAccountContactRetrieve() throws Exception {
		
		Contact contact = new Contact();
		Account account = new Account();
		account.setCompanyName("Atlassian");
		repoAccount.save(account);
		contact.setContactName("Test1");
		contact.setCity("Austin");
		contact.setAccount(account);
		repoContact.save(contact);
		Iterable<Contact> retrievedContants = repoContact.findAllByAccountCompanyName("Atlassian");
		assertThat(retrievedContants).hasOnlyElementsOfType(Contact.class);
		
		
	}
	@After
	public void flush(){
		
	}
}
