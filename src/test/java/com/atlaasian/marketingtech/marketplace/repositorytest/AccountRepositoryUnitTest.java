package com.atlaasian.marketingtech.marketplace.repositorytest;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.atlassian.marketingtech.marketplace.AtlassianApplication;
import com.atlassian.marketingtech.marketplace.domain.Account;
import com.atlassian.marketingtech.marketplace.repository.AccountRepository;
/**
 * 
 * @author selumalai
 * Class to Test Account repository
 */
@ContextConfiguration(classes = {AtlassianApplication.class})
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class AccountRepositoryUnitTest {
	
	@Autowired
	private AccountRepository repoAccount;
	
	@Before
	public void setUp(){
		
	}

	@Test
	public void testAccountPersist() throws Exception {
		Account account = new Account();
		account.setCompanyName("Atlassian");
		Account retrievedAccount = repoAccount.save(account);
		assertThat(retrievedAccount).hasFieldOrPropertyWithValue("companyName","Atlassian");
	}
	
	@Test
	public void testContactRetrieve() throws Exception {
		Account account = new Account();
		account.setCompanyName("Test1");
		account.setCity("Austin");
		repoAccount.save(account);
		Account retrievedAccount = repoAccount.findByCompanyName("Test1");
		assertThat(retrievedAccount).hasFieldOrPropertyWithValue("city","Austin");
	}
	
	@After
	public void flush(){
		
	}
}
