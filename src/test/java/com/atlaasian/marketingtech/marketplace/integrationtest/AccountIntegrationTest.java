package com.atlaasian.marketingtech.marketplace.integrationtest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.atlassian.marketingtech.marketplace.AtlassianApplication;
import com.atlassian.marketingtech.marketplace.domain.Account;
import com.atlassian.marketingtech.marketplace.repository.AccountRepository;
/**
 * 
 * @author selumalai
 * Class to do Integration Test Account controller, service, repository
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = AtlassianApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application.properties")
public class AccountIntegrationTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private AccountRepository repoAccount;

	@Test
	public void accountGetPostTest() throws Exception {
		String payload = "{\"companyName\":\"Atlassian\",\"addressLine1\":\"303 Colorado st\",\"addressLine2\":\"#1600\",\"city\":\"Austin\",\"state\":\"Texas\",\"postalCode\":\"78701\",\"country\":\"United States\"}";
		mockMvc.perform(post("/account").contentType(MediaType.APPLICATION_JSON).content(payload))
				.andExpect(status().isOk());
		Account retrievedAccount = repoAccount.findByCompanyName("Atlassian");
		assertThat(retrievedAccount).hasFieldOrPropertyWithValue("city", "Austin");
		mockMvc.perform(get("/account").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().string("[" + payload + "]"));
		mockMvc.perform(get("/account/Atlassian").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().string(payload));

		String updatedPayload = "{\"companyName\":\"Atlassian\",\"addressLine1\":\"303 Colorado st\",\"addressLine2\":\"#1600\",\"city\":\"Austin\",\"state\":\"Texas\",\"postalCode\":\"78727\",\"country\":\"United States\"}";
		mockMvc.perform(put("/account/Atlassian").contentType(MediaType.APPLICATION_JSON).content(updatedPayload))
				.andExpect(status().isOk());

		mockMvc.perform(get("/account").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().string("[" + updatedPayload + "]"));
		mockMvc.perform(get("/account/Atlassian").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().string(updatedPayload));
		retrievedAccount = repoAccount.findByCompanyName("Atlassian");
		assertThat(retrievedAccount).hasFieldOrPropertyWithValue("postalCode", "78727");
	}

}
