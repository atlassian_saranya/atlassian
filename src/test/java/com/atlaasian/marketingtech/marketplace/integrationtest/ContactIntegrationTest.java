package com.atlaasian.marketingtech.marketplace.integrationtest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.atlassian.marketingtech.marketplace.AtlassianApplication;
import com.atlassian.marketingtech.marketplace.domain.Contact;
import com.atlassian.marketingtech.marketplace.repository.ContactRepository;
/**
 * 
 * @author selumalai
 * Class to do Integration Test Contact controller, service, repository
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = AtlassianApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application.properties")
public class ContactIntegrationTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ContactRepository repoContact;

	@Test
	public void contactGetPostPutTest() throws Exception {
		String payload = "{\"contactName\":\"AtlassianMarketPlace\",\"addressLine1\":\"303 Colorado st\",\"addressLine2\":\"#1600\",\"city\":\"Austin\",\"state\":\"Texas\",\"postalCode\":\"78701\",\"country\":\"United States\",\"email\":\"selumalai@gmail.com\"}";
		mockMvc.perform(post("/contact").contentType(MediaType.APPLICATION_JSON).content(payload))
				.andExpect(status().isOk());
		Contact retrievedContact = repoContact.findByContactName("AtlassianMarketPlace");
		assertThat(retrievedContact).hasFieldOrPropertyWithValue("city", "Austin");
		mockMvc.perform(get("/contact").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
		MvcResult result = mockMvc.perform(get("/contact/AtlassianMarketPlace").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();
		JSONObject json = new JSONObject(result.getResponse().getContentAsString());
		assertThat(json.get("contactName").equals("AtlassianMarketPlace"));

		String updatedPayload = "{\"companyName\":\"AtlassianMarketPlace\",\"addressLine1\":\"303 Colorado st\",\"addressLine2\":\"#1600\",\"city\":\"Austin\",\"state\":\"Texas\",\"postalCode\":\"78727\",\"country\":\"United States\",\"email\":\"selumalai@ea.com\"}";
		mockMvc.perform(put("/contact/AtlassianMarketPlace").contentType(MediaType.APPLICATION_JSON).content(updatedPayload))
				.andExpect(status().isOk());

		mockMvc.perform(get("/contact").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
		mockMvc.perform(get("/contact/AtlassianMarketPlace").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andReturn();
		json = new JSONObject(result.getResponse().getContentAsString());
		assertThat(json.get("email").equals("selumalai@ea.com"));
		retrievedContact = repoContact.findByContactName("AtlassianMarketPlace");
		assertThat(retrievedContact).hasFieldOrPropertyWithValue("postalCode", "78727");
	}
	
	@Test
	public void accountContactGetPostPutTest() throws Exception {
		String payload = "{\"companyName\":\"Atlassian\",\"addressLine1\":\"303 Colorado st\",\"addressLine2\":\"#1600\",\"city\":\"Austin\",\"state\":\"Texas\",\"postalCode\":\"78701\",\"country\":\"United States\"}";
		mockMvc.perform(post("/account").contentType(MediaType.APPLICATION_JSON).content(payload))
				.andExpect(status().isOk());
		mockMvc.perform(get("/account/Atlassian/contact").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
		String contentPayload = "{\"contactName\":\"AtlassianMarketing\",\"addressLine1\":\"303 Colorado st\",\"addressLine2\":\"#1600\",\"city\":\"Austin\",\"state\":\"Texas\",\"postalCode\":\"78701\",\"country\":\"United States\",\"email\":\"selumalai@gmail.com\"}";
		mockMvc.perform(post("/account/Atlassian/contact").contentType(MediaType.APPLICATION_JSON).content(contentPayload))
				.andExpect(status().isOk());
		Contact retrievedContact = repoContact.findByContactName("AtlassianMarketing");
		assertThat(retrievedContact).hasFieldOrPropertyWithValue("city", "Austin");
		String updatedPayload = "{\"companyName\":\"AtlassianMarketPlace\",\"addressLine1\":\"303 Colorado st\",\"addressLine2\":\"#1600\",\"city\":\"Austin\",\"state\":\"Texas\",\"postalCode\":\"78727\",\"country\":\"United States\",\"email\":\"selumalai@ea.com\"}";
		mockMvc.perform(put("/account/Atlassian/contact/AtlassianMarketing").contentType(MediaType.APPLICATION_JSON).content(updatedPayload))
				.andExpect(status().isOk());
		retrievedContact = repoContact.findByContactName("AtlassianMarketing");
		assertThat(retrievedContact).hasFieldOrPropertyWithValue("postalCode", "78727");
	}

}
