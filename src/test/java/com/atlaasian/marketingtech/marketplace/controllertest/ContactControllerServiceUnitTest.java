package com.atlaasian.marketingtech.marketplace.controllertest;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.atlassian.marketingtech.marketplace.service.ContactService;
/**
 * 
 * @author selumalai
 * Class to Test Contact controller and service
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ContactControllerServiceUnitTest.class)

@WebMvcTest
public class ContactControllerServiceUnitTest {
	@Autowired
	MockMvc mockMvc;

	@MockBean
	ContactService contactService;
	
	@Before
	public void setUp() throws Exception{
		
	}

	@Test
	public void TestContactControllerGetAPI() {
		try {
			Mockito.when(contactService.retrieveAllContacts()).thenReturn(Collections.EMPTY_LIST);
			MvcResult mvcResult = mockMvc
					.perform(MockMvcRequestBuilders.get("/contact").accept(MediaType.APPLICATION_JSON)).andReturn();
			Integer responseCode = mvcResult.getResponse().getStatus();
			assertThat(responseCode.toString().equals("200"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void TestContactControllerPostAPI() {
		try {
			String payload = "{\"contactName\": \"Amazon\",\"address1\": \"303 Colorado st\",\"address2\": \"#1600\",\"city\": \"Seattle\",\"state\": \"Washington\",\"postalCode\": \"78701\",\"country\": \"United States\"}";
			
			MvcResult mvcResult = mockMvc
					.perform(MockMvcRequestBuilders.post("/contact").contentType(MediaType.APPLICATION_JSON).content(payload)).andReturn();
			Integer responseCode = mvcResult.getResponse().getStatus();
			assertThat(responseCode.toString().equals("200"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
